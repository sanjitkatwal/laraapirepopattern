<?php
declare(strict_types=1);

namespace App\Modules\StudentsCoursesEnrollments;


class StudentsCoursesEnrollments
{
    private $id;
    private $studentId;
    private $coursesId;
    private $enrolledByUsersId;
    private $deletedAt;
    private $createdAt;
    private $updatedAt;


    public function __construct($id, $studentId, $coursesId, $enrolledByUsersId, $deletedAt, $createdAt, $updatedAt)
    {
        $this->id = $id ?? null;
        $this->studentId = $studentId ?? null;
        $this->coursesId = $coursesId;
        $this->enrolledByUsersId = $enrolledByUsersId;
        $this->deletedAt = $deletedAt ?? null;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt ?? null;
    }

    public function toArray(): array
    {
        return [
            "id"                    => $this->id,
            "studentsId"            => $this->studentsId,
            "coursesId"             => $this->coursesId,
            "enrolledByUsersId"     => $this->enrolledByUsersId,
            "deletedAt"             => $this->deletedAt,
            "createdAt"             => $this->createdAt,
            "updatedAt"             => $this->updatedAt,
        ];
    }

    public function toSQL(): array
    {
        return [
            "id"                     => $this->id,
            "students_id"            => $this->studentsId,
            "courses_id"             => $this->coursesId,
            "enrolled_by_users_id"   => $this->enrolledByUsersId,
            "deleted_at"             => $this->deletedAt,
            "created_at"             => $this->createdAt,
            "updated_at"             => $this->updatedAt,
        ];
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStudentsId(): int
    {
        return $this->studentId;
    }

    public function getCapacity(): int
    {
        return $this->capacity;
    }

    public function getCoursesId(): int
    {
        return $this->coursesId;
    }

    public function getEnrolledByUsersId(): int
    {
        return $this->enrolledByUsersId;
    }
    public function getCreatedAt(): string
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?string
    {
        return $this->updatedAt;
    }

    public function getTotalStudentsEnrolled() : int
    {
        return $this->getTotalStudentsEnrolled;
    }

}
