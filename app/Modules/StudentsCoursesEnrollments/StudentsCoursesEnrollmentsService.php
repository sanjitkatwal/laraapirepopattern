<?php
declare(strict_types=1);

namespace App\Modules\StudentsCoursesEnrollments;

use App\Modules\Courses\Courses;
use App\Modules\Courses\CoursesMapper;
use Illuminate\Support\Facades\Auth;

class StudentsCoursesEnrollmentsService
{
    private $validator;
    private $repository;

    public function __construct(StudentsCoursesEnrollmentsValidator $validator, StudentsCoursesEnrollmentsRepository $repository)
    {
        $this->validator = $validator;
        $this->repository = $repository;
    }

    public function get(int $id) :StudentsCoursesEnrollments
    {
        return $this->repository->get($id);
    }

    public function update(array $data) : Courses
    {
        $data = array_merge_recursive(
            $data,
            [
                "enrolledByUserId" =>Auth::user()->id,
            ],
        );
        $this->validator->validateUpdate($data);
        return $this->repository->update(
            CoursesMapper::mapFrom($data)
        );
    }

    public function softDelete(int $id): bool
    {
        return $this->repository->softDelete($id);
    }
}
