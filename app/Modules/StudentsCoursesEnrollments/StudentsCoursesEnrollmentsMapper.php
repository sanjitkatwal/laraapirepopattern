<?php
declare(strict_types=1);

namespace App\Modules\StudentsCoursesEnrollments;


use App\Modules\Common\MyHelper;
use App\Modules\StudentsCoursesEnrollments\StudentsCoursesEnrollments;

class StudentsCoursesEnrollmentsMapper
{
    public static function mapFrom(array $data):StudentsCoursesEnrollments
    {
        return new Courses(
          //$data["id"] ?? null,
        MyHelper::nullStringToInt($data["id"] ?? null),
          $data["studentsId"],
          $data["coursesId"],
          $data["enrolledByUsersId"],
          $data["totalStudentEnrolled"],
          $data["deletedAt"] ?? null,
          $data["createdAt"] ?? date("Y-m-d H:i:s"),
          $data["updatedAt"] ?? null,
        );

   }
}
