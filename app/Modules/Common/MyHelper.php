<?php
declare(strict_types=1);

namespace App\Modules\Common;

abstract class MyHelper
{
    public static function nullStringToInt($str) : ?int //return nullable int
    {
        if($str !== null){
            return (int)$str; //convert it to integer
        }

        return null;
    }
}
