<?php
declare(strict_types=1);

namespace App\Modules\Students;


class Students
{
    private $id;
    private $name;
    private $email;
    private $deletedAt;
    private $createdAt;
    private $updatedAt;


    public function __construct($id, $name, $email, $deletedAt, $createdAt, $updatedAt)
    {
        $this->id = $id ?? null;
        $this->name = $name;
        $this->email = $email;
        $this->deletedAt = $deletedAt ?? null;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt ?? null;
    }

    public function toArray(): array
    {
        return [
            "id"            => $this->id,
            "name"          => $this->name,
            "email"         => $this->email,
            "deletedAt"     => $this->deletedAt,
            "createdAt"     => $this->createdAt,
            "updatedAt"     => $this->updatedAt,
        ];
    }

    public function toSQL(): array
    {
        return [
            "id"            => $this->id,
            "name"          => $this->name,
            "email"         => $this->email,
            "deleted_at"    => $this->deletedAt,
            "created_at"    => $this->createdAt,
            "updated_at"    => $this->updatedAt,
        ];
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getDeletedAt(): ?string
    {
        return $this->deletedAt;
    }

    public function getCreatedAt(): string
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?string
    {
        return $this->updatedAt;
    }

}
