<?php
declare(strict_types=1);

namespace App\Modules\Courses;


class Courses
{
    private $id;
    private $name;
    private $capacity;
    private $totalStudentEnrolled;
    private $deletedAt;
    private $createdAt;
    private $updatedAt;


    public function __construct($id, $name, $capacity, $totalStudentEnrolled, $deletedAt, $createdAt, $updatedAt)
    {
        $this->id = $id ?? null;
        $this->name = $name;
        $this->capacity = $capacity;
        $this->totalStudentEnrolled = $totalStudentEnrolled ?? null;
        $this->deletedAt = $deletedAt ?? null;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt ?? null;
    }

    public function toArray(): array
    {
        return [
            "id"            => $this->id,
            "name"          => $this->name,
            "capacity"      => $this->capacity,
            "totalStudentEnrolled" => $this->totalStudentEnrolled,
            "deletedAt"     => $this->deletedAt,
            "createdAt"     => $this->createdAt,
            "updatedAt"     => $this->updatedAt,
        ];
    }

    public function toSQL(): array
    {
        return [
            "id"            => $this->id,
            "name"          => $this->name,
            "capacity"      => $this->capacity,
            "deleted_at"    => $this->deletedAt,
            "created_at"    => $this->createdAt,
            "updated_at"    => $this->updatedAt,
        ];
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getCapacity(): int
    {
        return $this->capacity;
    }

    public function getDeletedAt(): ?string
    {
        return $this->deletedAt;
    }

    public function getCreatedAt(): string
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?string
    {
        return $this->updatedAt;
    }

    public function getTotalStudentsEnrolled() : int
    {
        return $this->getTotalStudentsEnrolled;
    }

}
