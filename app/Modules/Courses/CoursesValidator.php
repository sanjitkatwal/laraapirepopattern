<?php
declare(strict_types=1);

namespace App\Modules\Courses;
use InvalidArgumentException;

class CoursesValidator
{
    public function validateUpdate(array $rawData): void
    {
        $validator = \validator($rawData, [
            "name"      => 'required|string',
            "capacity"  => 'required|int',
        ]);

        if($validator->fails()){
            throw new \InvalidArgumentException(json_encode($validator->errors()->all()));
        }
    }
}
