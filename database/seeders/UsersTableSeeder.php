<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = array(
            array(
                'name' =>'Admin User',
                'email' => 'admin@gmail.com',
//                'password' => Hash:make('admin@123'),
                'password' => bcrypt('admin@123'),
                'status' => 'active',
                'role' => 'admin'
            )
        );

        foreach ($users as $user_data){
            if(User::where('email', $user_data['email']) ->count() <= 0){
                $user = new User;
                $user->fill($user_data);
                $user->save();
            }
        }
    }
}
