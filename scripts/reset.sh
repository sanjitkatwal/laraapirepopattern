#!/bin/sh
# chmod u+x YourScript.sh


echo "\nResetting up project ... \n"

echo "\nClearing cache ... \n"
php artisan clear
php artisan config:clear
php artisan cache:clear
php artisan view:clear
php artisan route:clear
php artisan db:seed

echo "\nDropping/recreating database"
php artisan migrate:fresh

echo "\nDone :)\n"
